const routers = [{
		path: '/',
		component: (resolve) => require(['./views/login.vue'], resolve)
	},
	{
		path: '/root',
		component: (resolve) => require(['./views/root.vue'], resolve),
		children: [
		    {
		    	path:'/',
		    	redirect: '/index'
		    },
		    {
				path: '/index',
				component: (resolve) => require(['./views/index.vue'], resolve)
			},
			{
				path: '/integral',
				component: (resolve) => require(['./views/integral.vue'], resolve)
			},
			{
				path: '/integralshop',
				component: (resolve) => require(['./views/integralshop.vue'], resolve)
			},
			{
				path: '/testinline',
				component: (resolve) => require(['./views/testinline.vue'], resolve)
			},
			{
				path: '/testhistory',
				component: (resolve) => require(['./views/testhistory.vue'], resolve)
			},
			//添加党建活动
			{
				path: '/activity',
				component: (resolve) => require(['./views/activity.vue'], resolve)
			},
			//党建活动列表
			{
				path: '/partyactivity',
				component: (resolve) => require(['./views/partyactivity.vue'], resolve)
			},
			{
				path: '/train',
				component: (resolve) => require(['./views/train.vue'], resolve)
			},
			{
				path: '/trainhistory',
				component: (resolve) => require(['./views/trainhistory.vue'], resolve)
			},
			{
				path: '/history',
				component: (resolve) => require(['./views/history.vue'], resolve)
			},
			{
				path: '/beyond',
				component: (resolve) => require(['./views/beyond.vue'], resolve)
			},
			{
				path: '/lecture',
				component: (resolve) => require(['./views/lecture.vue'], resolve)
			},
			{
				path: '/lecturehistory',
				component: (resolve) => require(['./views/lecturehistory.vue'], resolve)
			},
		]
	},

];
export default routers;